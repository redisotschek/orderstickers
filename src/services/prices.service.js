import axios from 'axios'

export default class ApiPricesService {

  static getPrices () {
    return axios.get(
      'http://localhost:3000/prices'
    )
  }

  static createOrder (order) {
    return axios.post(
      'http://localhost:3000/prices/newOrder',
      order
    )
  }
}
